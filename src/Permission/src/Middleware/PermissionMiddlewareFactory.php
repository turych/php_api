<?php

namespace Permission\Middleware;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class PermissionMiddlewareFactory
{
	
	public function __invoke(ContainerInterface $container)
    {
        return new PermissionMiddleware();
    }
}