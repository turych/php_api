<?php

namespace Auth\Middleware;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

use Auth\Account\AccountServiceInterface;

class AuthenticationMiddleware implements ServerMiddlewareInterface
{
    private $accountService;

    public function __construct(AccountServiceInterface $accountService)
    {
        $this->accountService = $accountService;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        if ($request->hasHeader('Authorization')) {
            $authentication_header = explode(' ', $request->getHeader('Authorization')[0]);
            if(count($authentication_header) == 2) {
                $auth_type = $authentication_header[0];
                $token = $authentication_header[1];
            } else {
                return new JsonResponse(
                    ['errors' => ['Request headers is fail.']],
                    400
                );
            }
        } else {
            $auth_type = $token = null;
        }

        if ($request->hasHeader('X-Leonis-Api-Key')) {
            $this->accountService->getIdentity()->setApiKey($request->getHeader('X-Leonis-Api-Key')[0]);
        }
        $this->accountService->setAuthType($auth_type);

        if ($request->hasHeader('X-Leonis-Api-Refresh-Token')) {
            $token = $request->getHeader('X-Leonis-Api-Refresh-Token')[0];
            $this->accountService->refresh(true);
        }
        if ($request->hasHeader('X-Leonis-Api-Auth-Sms-Pin')) {
            $this->accountService->setSmsPin($request->getHeader('X-Leonis-Api-Auth-Sms-Pin')[0]);
        }

        $this->accountService->getIdentity()->setToken($token);
        $this->accountService->authenticate();

        if (!$this->accountService->hasIdentity()) {
            return new JsonResponse(
                ['errors' => $this->accountService->getErrorMessages()],
                401
            );
        }

        if ($this->accountService->refresh()) {
            return new JsonResponse(
                [
                    'token' => $this->accountService->getIdentity()->getToken(),
                    'expires' => [
                        'date' => $this->accountService->getIdentity()->getExpires()
                    ]
                ]
            );
        }
       
        $identity = $this->accountService->getIdentity();

        return $delegate->process($request->withAttribute(self::class, $identity));
    }
}