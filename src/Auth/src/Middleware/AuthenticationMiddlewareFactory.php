<?php

namespace Auth\Middleware;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

use Auth\Account\AccountServiceInterface;

class AuthenticationMiddlewareFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $accountService = $container->get(AccountServiceInterface::class);
        
        return new AuthenticationMiddleware($accountService);
    }
}