<?php
//TODO create auth adapters:Basic, Bearer, Sms
namespace Auth\Account;

use Auth\Entity\Account;
use Auth\Repository\AccountAuthenticationInterface;

class AccountService implements AccountServiceInterface
{
    /**
     * Time for which the token is updated. Format DateInterval.
     */
    const REFRESH_TOKEN_TIME = 'P10D';

    /**
     * @var Account
     */
    private $account;

    /**
     * Basic or Bearer.
     * @var string
     */
    private $auth_type;

    /**
     * @var AccountAuthenticationInterface
     */
    private $accountRepository;

    /**
     * @var bool
     */
    private $refresh = false;

    /**
     * @var array
     */
    private $errors = [];

    public function __construct(
        Account $account, 
        AccountAuthenticationInterface $accountRepository
    )
    {
        $this->account = $account;
        $this->accountRepository = $accountRepository;
    }

    public function authenticate()
    {
        //Check Api Key.
        $account = $this->accountRepository->authenticateApiKey($this->getIdentity()->getApiKey());

        if (is_null($account)) {
            $this->setAuthFail("Api Key is not valid.");
            return false;
        }

        switch ($this->getAuthType()) {
            case 'Basic':

                $this->refresh(true);
                $auth_data = explode(':', base64_decode($this->getIdentity()->getToken()));
                if (count($auth_data) == 2) {
                    $this->getNewToken($auth_data);
                } else {
                    $this->setAuthFail('Authentication fail.');
                }

                break;

            case 'Bearer':

                //Check expires time
                if (new \DateTime($account->getExpires()) < new \DateTime()) {
                    $this->setAuthFail("Token is expired.");
                    return false;
                }

                // Check token
                if ($account->getToken() != $this->getIdentity()->getToken()) {
                    $this->setAuthFail("Token is not valid.");
                    return false;
                }

                $this->account = $account;

                if ($this->refresh()) {
                    $this->refreshToken();
                }

                break;

            case 'Sms':

                // TODO: all sms logic move to adapter

                if ($this->sms_pin == '0000') {

                    $smsProvider = new \TurboSms\TurboSms(
                        [
                            'login' => 'eltorggateT5',
                            'password' => 'go68SMS31'
                        ],
                        'soap'
                    );

                    $sms = [
                        'sender' => 'ElectroTorg',
                        'destination' => '+' . base64_decode($this->getIdentity()->getToken()),
                        'text' => sprintf("%04d", mt_rand(1, 9999))
                    ];

                    $result = $smsProvider->send($sms);

                    if ($result->SendSMSResult->ResultArray[0] != 'Сообщения успешно отправлены') {
                        throw new \Exception($result->SendSMSResult->ResultArray[0]);
                    }

                    $account->setSmsPin($sms['text']);
                    $date = new \DateTime();
                    $date->add(new \DateInterval('PT5M'));
                    $account->setSmsPinExpires($date->format('Y-m-d H:i:s'));

                    $this->account = $account;
                    $this->accountRepository->setSmsPin($this->getIdentity());

                } else {
                    $this->account = $account;
                    $now = new \DateTime;
                    $sms_pin_expires = new \DateTime($this->getIdentity()->getSmsPinExpires());
                    if ($now > $sms_pin_expires) {
                        $this->setAuthFail("Sms pin is expired.");
                        return false;
                    }
                    $this->refreshToken();
                    $this->refresh(true);
                }

                break;

            default:
                $this->setAuthFail("Authentication fail.");
                break;
        }
    }

    /**
     * Generate new token and write to db. Needs login and password data.
     * 
     * @param  array  $auth_data [0 => login, 1 => password]
     *
     * @return bool
     */
    public function getNewToken(array $auth_data)
    {
        if (!is_null($auth_data[0]) && !is_null($auth_data[1])) {

            $this->getIdentity()->setLogin($auth_data[0]);
            $this->getIdentity()->setPassword(md5($auth_data[1]));

            $account = $this->accountRepository->authenticateAccount($this->getIdentity());

            if (is_null($account->getContactId())) {
                $this->setAuthFail("Auth data is incorrect.");
                return false;
            }

            $this->getIdentity()->setContactId($account->getContactId());
            $this->getIdentity()->setToken($this->generateToken());
            $this->getIdentity()->setExpires($this->genereteExpires());

            $this->accountRepository->setToken($this->getIdentity());
        }
    }

    /**
     * Refresh token.
     */
    protected function refreshToken()
    {
        $this->getIdentity()->setToken($this->generateToken());
        $this->getIdentity()->setExpires($this->genereteExpires());
        $this->accountRepository->setToken($this->getIdentity());
    }

    /**
     * Check token of user.
     * 
     * @return boolean 
     */
    public function hasIdentity() : bool
    {
        return !is_null($this->getIdentity()->getToken());
    }

    /**
     * Return user data.
     * 
     * @return Account
     */
    public function getIdentity() : Account
    {
        return $this->account;
    }

    private function generateToken()
    {
        return bin2hex(random_bytes(16));
    }

    private function genereteExpires()
    {
        $now = new \DateTime();
        return $now->add(new \DateInterval(self::REFRESH_TOKEN_TIME));
    }

    /**
     * @return string
     */
    public function getAuthType()
    {
        return $this->auth_type;
    }

    /**
     * @param string $auth_type
     */
    public function setAuthType($auth_type)
    {
        $this->auth_type = $auth_type;
    }

    /**
     * @param $refresh
     * @return bool
     */
    public function refresh($refresh = null)
    {
        if (!is_null($refresh)) {
            $this->refresh = $refresh;
        }
        return $this->refresh;
    }


    public function setErrorMessage($message)
    {
        $this->errors[] = $message;
    }

    public function setAuthFail($message)
    {
    	$this->getIdentity()->setToken(null);
        $this->setErrorMessage($message);
    }

    public function getErrorMessages()
    {
        return $this->errors;
    }

    public function setSmsPin($pin)
    {
        $this->sms_pin = $pin;
    }
}
