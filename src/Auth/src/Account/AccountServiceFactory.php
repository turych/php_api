<?php

namespace Auth\Account;

use Interop\Container\ContainerInterface;
use Auth\Entity\Account;
use Auth\Repository\AccountAuthenticationInterface;

class AccountServiceFactory
{
	public function __invoke(ContainerInterface $container)
	{
		return new AccountService(
			new Account,
            $container->get(AccountAuthenticationInterface::class)
		);
	}
}