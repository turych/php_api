<?php

namespace Auth\Account;

/**
 * Interface AccountServiceInterface
 * @package Auth\Account
 */
interface AccountServiceInterface
{
    /**
     * @return mixed
     */
    public function authenticate();

    /**
     * @return string
     */
    public function getAuthType();


    /**
     * @param string $auth_type
     */
    public function setAuthType($auth_type);
}