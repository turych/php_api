<?php

namespace Auth;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'routes' => $this->getRouteConfig(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'aliases' => [
                Account\AccountServiceInterface::class => Account\AccountService::class,
                Repository\AccountAuthenticationInterface::class => Repository\AccountRepository::class,
            ],
            'delegators' => [
            ],
            'invokables' => [
            ],
            'factories'  => [
                Middleware\AuthenticationMiddleware::class => Middleware\AuthenticationMiddlewareFactory::class,
                Account\AccountService::class => Account\AccountServiceFactory::class,
                Action\LoginPageAction::class => Action\LoginPageFactory::class,
                Repository\AccountRepository::class => Repository\AccountRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Provides the namespace's route configuration
     *
     * @return array
     */
    public function getRouteConfig()
    {
        return [];
    }
}
