<?php

namespace Auth\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class LoginPageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);

        return new LoginPageAction($router);
    }
}
