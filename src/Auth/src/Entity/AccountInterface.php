<?php

namespace Auth\Entity;

/**
 * Interface AccountInterface
 * @package Auth\Entity
 */
interface AccountInterface
{
    /**
     * @return string
     */
    public function getApiKey(): string;

    /**
     * @param string $api_key
     */
    public function setApiKey(string $api_key);

    /**
     * @return string
     */
    public function getToken();

    /**
     * @param string $token
     */
    public function setToken($token);

    /**
     * @return string|\DateTime
     */
    public function getExpires();

    /**
     * @param string|\DateTime $expires
     */
    public function setExpires($expires);

    /**
     * @return int
     */
    public function getContactId(): int;

    /**
     * @param int $contact_id
     */
    public function setContactId(int $contact_id);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name);

    /**
     * @return int|string|array
     */
    public function getRole();

    /**
     * @param int|string|array $role
     */
    public function setRole($role);

    /**
     * @return string
     */
    public function getLogin(): string;

    /**
     * @param string $login
     */
    public function setLogin(string $login);

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @param string $password
     */
    public function setPassword(string $password);
}