<?php 

namespace Auth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Account
 * @package Auth\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="api_token")
 */
class Account implements AccountInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="contact_id", type="integer")
     * @var int
     */
    private $contact_id;

    /**
     * @ORM\Column(name="api_key", type="string")
     * @var string
     */
    private $api_key;

    /**
     * @ORM\Column(name="token", type="string")
     * @var string
     */
    private $token;

    /**
     * @ORM\Column(name="sms_pin", type="integer")
     * @var int
     */
    private $sms_pin;

    /**
     * @ORM\Column(name="sms_pin_expires", type="string")
     * @var string
     */
    private $sms_pin_expires;

    /**
     * @ORM\Column(name="expires", type="string", length=32)
     * @var string
     */
    private $expires;

    /**
     * @ORM\Column(name="name", type="string", length=150)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=50)
     * @var mixed
     */
    private $role;

    /**
     * @ORM\Column(name="login", type="string", length=32)
     * @var string
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="string", length=128)
     * @var string
     */
    private $password;


    /**
     * {@inheritdoc}
     */
    public function setApiKey(string $api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * {@inheritdoc}
     */
    public function getApiKey(): string
    {
        return $this->api_key;
    }

    /**
     * {@inheritdoc}
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return int
     */
    public function getSmsPin(): int
    {
        return $this->sms_pin;
    }

    /**
     * @param int $sms_pin
     */
    public function setSmsPin(int $sms_pin)
    {
        $this->sms_pin = $sms_pin;
    }

    /**
     * @return string
     */
    public function getSmsPinExpires(): string
    {
        return $this->sms_pin_expires;
    }

    /**
     * @param string $sms_pin_expires
     */
    public function setSmsPinExpires(string $sms_pin_expires)
    {
        $this->sms_pin_expires = $sms_pin_expires;
    }

    /**
     * {@inheritdoc}
     */
    public function setExpires($expires)
    {
        if ($expires instanceof \DateTime) {
            $this->expires = $expires->format("Y-m-d H:i:s");
        } else {
            $this->expires = $expires;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * {@inheritdoc}
     */
    public function setContactId(int $contact_id)
    {
        $this->contact_id = $contact_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getContactId() : int
    {
        return $this->contact_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * {@inheritdoc}
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * {@inheritdoc}
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

}