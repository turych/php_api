<?php 

namespace Auth\Repository;

use Doctrine\ORM\EntityManager;
use LeonisApi\Entity\Contact;
use Auth\Entity\Account;

class AccountRepository implements AccountAuthenticationInterface
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticateAccount(Account $account)
    {
        $query = $this->entityManager->createQueryBuilder()
            ->select('c')
            //TODO: change from to Account::class, fill account data in DB
            ->from(Contact::class, 'c')
            ->where('c.login = :login', 'c.password = :password')
            ->setParameters([
                    'login' => $account->getLogin(),
                    'password'=> $account->getPassword()
                ])
            ->getQuery()
        ;

        $result = $query->getOneOrNullResult();

        $account->setContactId($result->getId());
        $account->setRole($result->getRoleId());
        $account->setName($result->getFirstname());

        return is_null($account) ? null : $account;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticateApiKey($api_key)
    {
        return $this->entityManager
            ->getRepository(Account::class)
            ->findOneBy(['api_key' => $api_key]);
    }

    /**
     * {@inheritdoc}
     */
    public function setToken(Account $account)
    {
        $this->entityManager->merge($account);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function setSmsPin(Account $account)
    {
        $this->entityManager->merge($account);
        $this->entityManager->flush();
    }
}