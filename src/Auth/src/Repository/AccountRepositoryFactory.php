<?php 

namespace Auth\Repository;

use Interop\Container\ContainerInterface;

class AccountRepositoryFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new AccountRepository(
            $container->get('doctrine.entity_manager.orm_default')
        );
    }
}