<?php
namespace Auth\Repository;

use Auth\Entity\Account;

interface AccountAuthenticationInterface
{
    /**
     * @param Account $account
	 *
     * @return Account
     */
    public function authenticateAccount(Account $account);

    /**
     * @param string $api_key
     *
     * @return Account
     */
    public function authenticateApiKey($api_key);

    /**
     * @param Account $account
     */
    public function setToken(Account $account);
}