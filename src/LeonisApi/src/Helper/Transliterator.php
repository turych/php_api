<?php

namespace LeonisApi\Helper;

class Transliterator
{
    protected static $words = ['а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''];

    public static function transliterate(string $str) : string
    {
        $str = strip_tags($str); // remove HTML-tags
        $str = str_replace(array("\n", "\r"), " ", $str);
        $str = preg_replace("/\s+/", ' ', $str); // remove repeated spaces
        $str = trim($str);
        $str = function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
        $str = strtr($str, self::$words);
        $str = preg_replace("/[^0-9a-z-_ ]/i", "", $str); // remove symbols
        $str = str_replace(" ", "-", $str); // replace space to "-"
        return $str;
    }
}