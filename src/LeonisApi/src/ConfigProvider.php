<?php

namespace LeonisApi;

use LeonisApi\Action\Factory\ActionFactory;
use LeonisApi\Action\Order\OrderAction;
use LeonisApi\Action\Order\OrderListAction;
use LeonisApi\Action\Order\OrderUpdateAction;
use LeonisApi\Repository\ContactRepository;
use LeonisApi\Repository\OrderRepository;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'routes' => $this->getRouteConfig(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
            ],

            // TODO: write function for get all Action classes.
            'factories'  => [
                Action\HomePageAction::class => Action\Factory\ActionFactory::class,
                // Statistics
                Action\CallLogAction::class => Action\Factory\ActionFactory::class,
                Action\CallRecordAction::class => Action\Factory\ActionFactory::class,
                Action\SmsLogAction::class => Action\Factory\ActionFactory::class,
                // Contact
                Action\ContactAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactEmailAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactPhoneAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactImAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactUrlAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactSocialnetworkAction::class => Action\Factory\ActionFactory::class,
                Action\ContactData\ContactAddressAction::class => Action\Factory\ActionFactory::class,
                //Product
                Action\Product\ProductListAction::class => Action\Factory\ActionFactory::class,
                Action\Product\ProductAction::class => Action\Factory\ActionFactory::class,
                Action\Product\ProductSkuAction::class => Action\Factory\ActionFactory::class,
                Action\Product\ProductStockAction::class => Action\Factory\ActionFactory::class,
                // Category
                Action\CategoryAction::class => Action\Factory\ActionFactory::class,
                Action\CategoryListAction::class => Action\Factory\ActionFactory::class,
                // Order
                OrderAction::class => ActionFactory::class,
                OrderListAction::class => ActionFactory::class,
                OrderRepository::class => ActionFactory::class,
                OrderUpdateAction::class => ActionFactory::class,
            ],
        ];
    }


    // TODO: [may be] write route config in actions??
   /**
     * Provides the namespace's route configuration
     *
     * @return array
     */
    public function getRouteConfig()
    {
        return [
            [
                'name' => 'home',
                'path' => '/',
                'middleware' => Action\HomePageAction::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'contact',
                'path' => '/contact[/{id:\d+}]',
                'middleware' => Action\ContactAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_email',
                'path' => '/contact/{id:\d+}/email',
                'middleware' => Action\ContactData\ContactEmailAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_phone',
                'path' => '/contact/{id:\d+}/phone',
                'middleware' => Action\ContactData\ContactPhoneAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_im',
                'path' => '/contact/{id:\d+}/im',
                'middleware' => Action\ContactData\ContactImAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_url',
                'path' => '/contact/{id:\d+}/url',
                'middleware' => Action\ContactData\ContactUrlAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_socialnetwork',
                'path' => '/contact/{id:\d+}/socialnetwork',
                'middleware' => Action\ContactData\ContactSocialnetworkAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'contact_address',
                'path' => '/contact/{id:\d+}/address',
                'middleware' => Action\ContactData\ContactAddressAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'call_log',
                'path' => '/call/log',
                'middleware' => Action\CallLogAction::class,
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'call_record',
                'path' => '/call/record[/{id}]',
                'middleware' => Action\CallRecordAction::class,
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'sms_log',
                'path' => '/sms/log',
                'middleware' => Action\SmsLogAction::class,
                'allowed_methods' => ['POST'],
            ],
            //Product
            [
                'name' => 'product_list',
                'path' => '/product/list',
                'middleware' => Action\Product\ProductListAction::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'product',
                'path' => '/product[/{product_id:\d+}]',
                'middleware' => Action\Product\ProductAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT'],
            ],
            [
                'name' => 'product_sku',
                'path' => '/product/{product_id:\d+}/sku[/{sku_id:\d+}]',
                'middleware' => Action\Product\ProductSkuAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT'],
            ],
            [
                'name' => 'product_sku_stock',
                'path' => '/product/{product_id:\d+}/sku/{sku_id:\d+}/stock[/{stock_id:\d+}]',
                'middleware' => Action\Product\ProductStockAction::class,
                'allowed_methods' => ['GET', 'PUT', 'DELETE'],
            ],
            // Category
            [
                'name' => 'category',
                'path' => '/category/{id:\d+}',
                'middleware' => Action\CategoryAction::class,
                'allowed_methods' => ['GET', 'POST', 'PUT', 'DELETE'],
            ],
            [
                'name' => 'category_list',
                'path' => '/category/list',
                'middleware' => Action\CategoryListAction::class,
                'allowed_methods' => ['GET'],
            ],
            // Order
            [
                'name' => 'order_list',
                'path' => '/order/list',
                'middleware' => OrderListAction::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'order',
                'path' => '/order/{id:\d+}',
                'middleware' => OrderAction::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'order_new',
                'path' => '/order',
                'middleware' => OrderAction::class,
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'update_item_in_order',
                'path' => '/order/{order_id:\d+}/item',
                'middleware' => OrderAction::class,
                'allowed_methods' => ['PUT'],
            ],
            [
                'name' => 'delete_item_to_order',
                'path' => '/order/{order_id:\d+}/item/{item_id:\d+}',
                'middleware' => OrderAction::class,
                'allowed_methods' => ['DELETE'],
            ],
        ];
    }
}
