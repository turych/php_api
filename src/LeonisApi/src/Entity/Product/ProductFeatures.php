<?php

namespace LeonisApi\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop_product_features")
 */

class ProductFeatures
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="product_id", type="integer")
     * @var int
     */
    private $product_id;

    /**
     * @ORM\Column(name="sku_id", type="integer")
     * @var int
     */
    private $sku_id;

    /**
     * @ORM\Column(name="feature_id", type="integer")
     * @var int
     */
    private $feature_id;

    /**
     * @ORM\Column(name="feature_value_id", type="integer")
     * @var int
     */
    private $feature_value_id;
}