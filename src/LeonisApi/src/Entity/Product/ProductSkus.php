<?php

namespace LeonisApi\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop_product_skus")
 */

class ProductSkus
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="product_id", type="integer")
     * @var int
     */
    private $product_id;

    /**
     * Sku code.
     *
     * @ORM\Column(name="sku", type="string", length=255)
     * @var string
     */
    private $sku;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="sort", type="integer")
     * @var int
     */
    private $sort;

    /**
     * @ORM\Column(name="image_id", type="integer")
     * @var int
     */
    private $image_id;

    /**
     * @ORM\Column(name="price", type="decimal")
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(name="primary_price", type="decimal")
     * @var float
     */
    private $primary_price;

    /**
     * @ORM\Column(name="purchase_price", type="decimal")
     * @var float
     */
    private $purchase_price;

    /**
     * @ORM\Column(name="count", type="integer")
     * @var int
     */
    private $count;

    /**
     * @ORM\Column(name="available", type="integer")
     * @var int
     */
    private $available;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId(int $product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getImageId(): int
    {
        return $this->image_id;
    }

    /**
     * @param int $image_id
     */
    public function setImageId(int $image_id)
    {
        $this->image_id = $image_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPrimaryPrice(): float
    {
        return $this->primary_price;
    }

    /**
     * @param float $primary_price
     */
    public function setPrimaryPrice(float $primary_price)
    {
        $this->primary_price = $primary_price;
    }

    /**
     * @return float
     */
    public function getPurchasePrice(): float
    {
        return $this->purchase_price;
    }

    /**
     * @param float $purchase_price
     */
    public function setPurchasePrice(float $purchase_price)
    {
        $this->purchase_price = $purchase_price;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getAvailable(): int
    {
        return $this->available;
    }

    /**
     * @param int $available
     */
    public function setAvailable(int $available)
    {
        $this->available = $available;
    }


}