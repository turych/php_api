<?php

namespace LeonisApi\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\ProductStocksRepository")
 * @ORM\Table(name="shop_product_stocks")
 */

class ProductStocks
{
    /**
     * @ORM\Id
     * @ORM\Column(name="sku_id", type="integer")
     * @var int
     */
    private $sku_id;

    /**
     * @ORM\Id
     * @ORM\Column(name="stock_id", type="integer")
     * @var int
     */
    private $stock_id;

    /**
     * @ORM\Column(name="product_id", type="integer")
     * @var int
     */
    private $product_id;

    /**
     * @ORM\Column(name="count", type="integer")
     * @var int
     */
    private $count;

    /**
     * @return int
     */
    public function getSkuId(): int
    {
        return $this->sku_id;
    }

    /**
     * @param int $sku_id
     */
    public function setSkuId(int $sku_id)
    {
        $this->sku_id = $sku_id;
    }

    /**
     * @return int
     */
    public function getStockId(): int
    {
        return $this->stock_id;
    }

    /**
     * @param int $stock_id
     */
    public function setStockId(int $stock_id)
    {
        $this->stock_id = $stock_id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId(int $product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }
}