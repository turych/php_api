<?php

namespace LeonisApi\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use LeonisApi\Entity\Order\OrderItems;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\ProductRepository")
 * @ORM\Table(name="shop_product")
 */

class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="status", type="integer")
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="image_id", type="integer")
     * @var int
     */
    private $image_id;

    /**
     * @ORM\Column(name="sku_id", type="integer")
     * @var int
     */
    private $sku_id;

    /**
     * @ORM\Column(name="url", type="string", length=255)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(name="rating", type="decimal", options={"default":"0"})
     * @var float
     */
    private $rating = 0;

    /**
     * @ORM\Column(name="currency", type="string")
     * @var string
     */
    private $currency;

    /**
     * @ORM\Column(name="price", type="decimal", options={"default":"0"})
     * @var float
     */
    private $price = 0;

    /**
     * @ORM\Column(name="min_price", type="decimal", options={"default":"0"})
     * @var float
     */
    private $min_price = 0;

    /**
     * @ORM\Column(name="max_price", type="decimal", options={"default":"0"})
     * @var float
     */
    private $max_price = 0;

    /**
     * @ORM\Column(name="count", type="integer")
     * @var int
     */
    private $count = null;

    /**
     * @ORM\Column(name="category_id", type="integer", options={"default":"null"})
     * @var int
     */
    private $category_id;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="\LeonisApi\Entity\Order\OrderItems", mappedBy="product_id")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getOrders()
    {
        return $this->orders;
    }


//    /**
//     * @ORM\ManyToMany(targetEntity="\LeonisApi\Entity\Order\Order", mappedBy="items")
//     */
//    private $orders;
//
//    public function __construct()
//    {
//        $this->orders = new ArrayCollection();
//    }

//    /**
//     * @return ArrayCollection
//     */
//    public function getOrders()
//    {
//        return $this->orders;
//    }

    public function getAsArray() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status,
            'image_id' => $this->image_id,
            'sku_id' => $this->sku_id,
            'url' => $this->url,
            'rating' => $this->rating,
            'currency' => $this->currency,
            'price' => $this->price,
            'min_price' => $this->min_price,
            'max_price' => $this->max_price,
            'count' => $this->count,
            'category_id' => $this->category_id,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getImageId(): int
    {
        return $this->image_id;
    }

    /**
     * @param int $image_id
     */
    public function setImageId(int $image_id)
    {
        $this->image_id = $image_id;
    }

    /**
     * @return int
     */
    public function getSkuId(): int
    {
        return $this->sku_id;
    }

    /**
     * @param int $sku_id
     */
    public function setSkuId(int $sku_id)
    {
        $this->sku_id = $sku_id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating(float $rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getMinPrice(): float
    {
        return $this->min_price;
    }

    /**
     * @param float $min_price
     */
    public function setMinPrice(float $min_price)
    {
        $this->min_price = $min_price;
    }

    /**
     * @return float
     */
    public function getMaxPrice(): float
    {
        return $this->max_price;
    }

    /**
     * @param float $max_price
     */
    public function setMaxPrice(float $max_price)
    {
        $this->max_price = $max_price;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id)
    {
        $this->category_id = $category_id;
    }

    public function addOrderItem(OrderItems $item)
    {
        if (!$this->orders->contains($item)) {
            $this->orders->add($item);
            $item->setItem($this);
        }

        return $this;
    }


}