<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\SmsLogRepository")
 * @ORM\Table(name="crm_sms_statistics")})
 */
class SmsLog
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="phone_id", type="string")
     * @var int
     */
    private $phone_id;

    /**
     * @ORM\Column(name="src", type="string")
     * @var string
     */
    private $src;

    /**
     * in|out
     *
     * @ORM\Column(name="type", type="string")
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(name="content", type="string")
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(name="datetime", type="string")
     * @var string
     */
    private $datetime;

    /**
     * @ORM\Column(name="gateway_id", type="string")
     * @var string
     */
    private $gateway_id = 'api';

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return new \DateTime($this->datetime);
    }

    /**
     * @return string
     */
    public function getGatewayId(): string
    {
        return $this->gateway_id;
    }


}