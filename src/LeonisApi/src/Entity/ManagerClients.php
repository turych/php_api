<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="crm_manager_clients")
 */
class ManagerClients
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="manager_id", type="integer")
     * @var int
     */
    private $manager_id;

    /**
     * @ORM\Column(name="contact_id", type="integer")
     * @var int
     */
    private $contact_id;

    /**
     * @return int
     */
    public function getManagerId(): int
    {
        return $this->manager_id;
    }

    /**
     * @param int $manager_id
     */
    public function setManagerId(int $manager_id)
    {
        $this->manager_id = $manager_id;
    }

    /**
     * @return int
     */
    public function getContactId(): int
    {
        return $this->contact_id;
    }

    /**
     * @param int $contact_id
     */
    public function setContactId(int $contact_id)
    {
        $this->contact_id = $contact_id;
    }


}