<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="contact_emails")
 */
class ContactEmails
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="contact_id", type="integer")
     * @var int
     */
    private $contact_id;

    /**
     * @ORM\Column(name="email", type="string", length=32)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="ext", type="string", length=32)
     * @var string
     */
    private $ext;

    /**
     * @ORM\Column(name="sort", type="integer")
     * @var string
     */
    private $sort;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getContactId(): int
    {
        return $this->contact_id;
    }

    /**
     * @param int $contact_id
     */
    public function setContactId(int $contact_id)
    {
        $this->contact_id = $contact_id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getExt(): string
    {
        return $this->ext;
    }

    /**
     * @param string $ext
     */
    public function setExt(string $ext)
    {
        $this->ext = $ext;
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort(string $sort)
    {
        $this->sort = $sort;
    }
}
