<?php

namespace LeonisApi\Entity\Feature;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop_feature")
 */

class Feature
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="parent_id", type="integer")
     * @var int
     */
    private $parent_id;

    /**
     * @ORM\Column(name="code", type="string", length=64)
     * @var string
     */
    private $code;

    /**
     * MySQL type ENUM ('public','hidden','private').
     *
     * @ORM\Column(name="status", type="string", options={"default":"public"})
     * @var string
     */
    private $status;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     * @var string
     */
    private $type;
}