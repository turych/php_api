<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.01.18
 * Time: 11:06
 */

namespace LeonisApi\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use LeonisApi\Entity\Contact;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\OrderRepository")
 * @ORM\Table(name="shop_order")
 */
class Order
{

    const REMINDER_ON = '1';
    const REMINDER_OFF = '0';

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Many Orders have One Contact
     * @ManyToOne(targetEntity="\LeonisApi\Entity\Contact", inversedBy="orders")
     * @JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact_id;


    /**
     * @ORM\Column(name="create_datetime", type="datetime")
     * @var string
     */
    private $create_datetime;

    /**
     * @ORM\Column(name="update_datetime", type="datetime", options={"default":"null"})
     * @var string
     */
    private $update_datetime;

    /**
     * @ORM\Column(name="state_id", type="string", length=32)
     * @var string
     */
    private $state_id = 'new';

    /**
     * @ORM\Column(name="total", type="decimal", options={"default":"null"})
     * @var float
     */
    private $total = null;

    /**
     * @ORM\Column(name="currency", type="string", length=3)
     * @var string
     */
    private $currency;

    /**
     * @ORM\Column(name="rate", type="decimal", options={"default":"1"})
     * @var float
     */
    private $rate = 1;

    /**
     * @ORM\Column(name="tax", type="decimal", options={"default":"0"})
     * @var float
     */
    private $tax = 0;

    /**
     * @ORM\Column(name="shipping", type="decimal", options={"default":"0"})
     * @var float
     */
    private $shipping = 0;

    /**
     * @ORM\Column(name="discount", type="decimal", options={"default":"0"})
     * @var float
     */
    private $discount = 0;

    /**
     * Many Orders have One Contact
     * @ManyToOne(targetEntity="\LeonisApi\Entity\Contact", inversedBy="orders")
     * @JoinColumn(name="assigned_contact_id", referencedColumnName="id")
     */
    private $assigned_contact_id = null;

    /**
     * @ORM\Column(name="paid_year", type="integer")
     * @var int
     */
    private $paid_year = null;

    /**
     * @ORM\Column(name="paid_quarter", type="integer")
     * @var int
     */
    private $paid_quarter = null;

    /**
     * @ORM\Column(name="paid_month", type="integer")
     * @var int
     */
    private $paid_month = null;

    /**
     * @ORM\Column(name="paid_date", type="date")
     * @var int
     */
    private $paid_date = null;

    /**
     * @ORM\Column(name="is_first", type="integer", length=1)
     * @var string
     */
    private $is_first = 0;


    /**
     * @ORM\Column(name="comment", type="string")
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="reminder", type="string")
     * @var string
     */
    private $reminder;

//    /**
//     * @ORM\ManyToMany(targetEntity="\LeonisApi\Entity\Product\Product", inversedBy="orders")
//     * @ORM\JoinTable(
//     *  name="shop_order_items",
//     *  joinColumns={
//     *      @ORM\JoinColumn(name="order_id", referencedColumnName="id")
//     *  },
//     *  inverseJoinColumns={
//     *      @ORM\JoinColumn(name="product_id", referencedColumnName="id")
//     *  }
//     * )
//     */
//    private $products;


    /**
     * @OneToMany(targetEntity="\LeonisApi\Entity\Order\OrderItems", mappedBy="order_id")
     */
    private $items;


    public function __construct()
    {
//        $this->products = new ArrayCollection();
        $this->items = new ArrayCollection();
    }


//    /**
//     * get base info about products attached to order from table shop_product
//     *
//     * @return ArrayCollection
//     */
//    public function getProducts()
//    {
//        return $this->products;
//    }

    public function addSum($value)
    {
        $this->total += $value;
    }

    public function changeSum($minus_value, $plus_value = 0)
    {
        $this->total = $this->total - $minus_value + $plus_value;
    }

    /**
     * get order items from shop_order_items table
     *
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return $this
     */
    public function setUpdateDatetime()
    {
        $this->update_datetime = new \DateTime("now");
        return $this;
    }

    /**
     * @return $this
     */
    public function setCreateDatetime()
    {
        $this->create_datetime = new \DateTime("now");
        return $this;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function setContact($contact)
    {
        $this->contact_id = $contact;
        return $this;
    }

    public function setAssignedContact($contact)
    {
        $this->assigned_contact_id = $contact;
        return $this;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
        return $this;
    }

    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    public function addOrderItem(OrderItems $item)
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setOrder($this);
        }

        return $this;
    }

    public function setReminder($status)
    {
        if (!in_array($status, [self::REMINDER_ON, self::REMINDER_OFF])) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->reminder = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAsArray()
    {
        return [
            'id' => $this->id,
            'contact_id' => $this->contact_id->getId(),
            'assigned_contact_id' => $this->assigned_contact_id->getId(),
            'create_datetime' => $this->create_datetime,
            'update_datetime' => $this->update_datetime,
            'currency' => $this->currency,
            'rate' => $this->rate,
            'total' => $this->total,
            'comment' => $this->comment,
            'discount' => $this->discount,
            'shipping' => $this->shipping,
            'tax' => $this->tax
        ];
    }

}