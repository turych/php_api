<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.01.18
 * Time: 11:01
 */

namespace LeonisApi\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use LeonisApi\Entity\Product\Product;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\OrderItemsRepository")
 * @ORM\Table(name="shop_order_items")
 */
class OrderItems
{

    const TYPE_PRODUCT = 'product';
    const TYPE_SERVICE = 'service';

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="\LeonisApi\Entity\Order\Order", inversedBy="items")
     * @JoinColumn(name="order_id", referencedColumnName="id", nullable=FALSE)
     */
    private $order_id;

    /**
     * @ManyToOne(targetEntity="\LeonisApi\Entity\Product\Product", inversedBy="orders")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=FALSE)
     */
    private $product_id;


    /**
     * @ORM\Column(name="name", type="string")
     * @var string
     */
    private $name;

    /**
     *
     * @ORM\Column(name="sku_id", type="integer")
     * @var int
     */
    private $sku_id;

    /**
     * @ORM\Column(name="sku_code", type="string")
     * @var string
     */
    private $sku_code;

    /**
     * @ORM\Column(name="type", type="string")
     * @var string
     */
    private $type;

    /**
     *
     * @ORM\Column(name="service_id", type="integer", nullable=TRUE)
     * @var int
     */
    private $service_id = null;

    /**
     *
     * @ORM\Column(name="service_variant_id", type="integer", nullable=TRUE)
     * @var int
     */
    private $service_variant_id = null;

    /**
     *
     * @ORM\Column(name="price", type="decimal", precision=15, scale=4)
     */
    private $price;

    /**
     *
     * @ORM\Column(name="quantity", type="integer")
     * @var int
     */
    private $quantity;

    /**
     *
     * @ORM\Column(name="parent_id", type="integer")
     * @var int
     */
    private $parent_id;

    /**
     *
     * @ORM\Column(name="stock_id", type="integer")
     * @var int
     */
    private $stock_id;

    /**
     *
     * @ORM\Column(name="purchase_price", type="decimal", precision=15, scale=4)
     * @var int
     */
    private $purchase_price;

    /**
     * @var ArrayCollection
     */
    protected $items;

    /**
     * @var ArrayCollection
     */
    protected $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    public function getSum() {
        return $this->quantity * $this->price;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setOrder(Order $order = null)
    {
        $this->order_id = $order;
        return $this;
    }

    public function setItem(Product $item = null)
    {
        $this->product_id = $item;
        return $this;
    }


    public function setType($type)
    {
        if (!in_array($type, [self::TYPE_PRODUCT, self::TYPE_SERVICE])) {
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setSkuId($id) {
        $this->sku_id = $id;
        return $this;
    }

    public function setSkuCode($code) {
        $this->sku_code = $code;
        return $this;
    }

    public function setServiceId($id) {
        $this->service_id = $id;
        return $this;
    }

    public function setServiceVariantId($id) {
        $this->service_variant_id = $id;
        return $this;
    }


    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    public function setParentId($id) {
        $this->parent_id = $id;
        return $this;
    }

    public function setStockId($id) {
        $this->stock_id = $id;
        return $this;
    }

    public function setPurchasePrice($price) {
        $this->purchase_price = $price;
        return $this;
    }

    public function getAsArray() {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id->getId(),
            'product_id' => $this->product_id->getId(),
            'name' => $this->name,
            'sku_id' => $this->sku_id,
            'sku_code' => $this->sku_code,
            'type' => $this->type,
            'service_id' => $this->service_id,
            'service_variant_id' => $this->service_variant_id,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'parent_id' => $this->parent_id,
            'stock_id' => $this->stock_id,
            'purchase_price' => $this->purchase_price,
        ];
    }

}