<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=150)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="firstname", type="string", length=50)
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=50)
     * @var string
     */
    private $lastname;

    /**
     * @ORM\Column(name="middlename", type="string", length=50)
     * @var string
     */
    private $middlename;

    /**
     * @ORM\Column(name="company", type="string", length=150)
     * @var string
     */
    private $company;

    /**
     * @ORM\Column(name="jobtitle", type="string", length=50)
     * @var string
     */
    private $jobtitle;

    /**
     * @ORM\Column(name="about", type="string")
     * @var string
     */
    private $about;

    /**
     * @ORM\Column(name="is_user", type="integer")
     * @var int
     */
    private $is_user = 0;

    /**
     * @ORM\Column(name="login", type="string", length=32)
     * @var string
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="string", length=128)
     * @var string
     */
    private $password = '';

    /**
     * @ORM\OneToOne(targetEntity="\LeonisApi\Entity\ContactRole", mappedBy="contact_id")
     * @var int
    */
    private $role_id;

    /**
     * @ORM\Column(name="status", type="string", length=50)
     * @var string
     */
    private $status = '';

    /**
     * @ORM\Column(name="create_app_id", type="string", length=32)
     * @var string
     */
    private $create_app_id = "api";

    /**
     * @ORM\Column(name="create_method", type="string", length=32)
     * @var string
     */
    private $create_method = "add";

    /**
     * @ORM\Column(name="create_contact_id", type="integer")
     * @var int
     */
    private $create_contact_id;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="\LeonisApi\Entity\Order\Order", mappedBy="contact_id")
     * @OneToMany(targetEntity="\LeonisApi\Entity\Order\Order", mappedBy="assigned_contact_id")
     */
    protected $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getOrders() {
        return $this->orders;
    }

    /**
     * Get Contact id.
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     */
    protected function setName()
    {
        $this->name = $this->getLastname() . ' ' . $this->getFirstname() . ' ' . $this->getMiddlename();
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname)
    {
        $this->firstname = $firstname;
        $this->setName();
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;
        $this->setName();
    }

    /**
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * @param string $middlename
     */
    public function setMiddlename(string $middlename)
    {
        $this->middlename = $middlename;
        $this->setName();
    }

    /**
     * @return int
    */
    public function getRoleId()
    {
        return $this->role_id->getRoleId();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getJobtitle()
    {
        return $this->jobtitle;
    }

    /**
     * @param string $jobtitle
     */
    public function setJobtitle(string $jobtitle)
    {
        $this->jobtitle = $jobtitle;
    }

    /**
     * @return string
     */
    public function getAbout(): string
    {
        return $this->about;
    }

    /**
     * @param string $about
     */
    public function setAbout(string $about)
    {
        $this->about = $about;
    }

    /**
     * @param int $create_contact_id
     */
    public function setCreateContactId(int $create_contact_id)
    {
        $this->create_contact_id = $create_contact_id;
    }

}
