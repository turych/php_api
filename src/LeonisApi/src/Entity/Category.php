<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop_category")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="left_key", type="integer")
     * @var int
     */
    private $left_key;

    /**
     * @ORM\Column(name="right_key", type="integer")
     * @var int
     */
    private $right_key;

    /**
     * @ORM\Column(name="depth", type="integer")
     * @var int
     */
    private $depth;

    /**
     * @ORM\Column(name="parent_id", type="integer")
     * @var int
     */
    private $parent_id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="url", type="string", length=255)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(name="count", type="integer")
     * @var int
     */
    private $count;

    /**
     * @ORM\Column(name="description", type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="status", type="integer")
     * @var int
     */
    private $status;
}
