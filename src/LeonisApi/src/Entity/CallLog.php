<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\CallLogRepository")
 * @ORM\Table(name="crm_call_log", indexes={@ORM\Index(name="src", columns={"src"}), @ORM\Index(name="dst", columns={"dst"})})
 */
class CallLog
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="phone_id", type="string")
     * @var string
     */
    private $phone_id;

    /**
     * @ORM\Column(name="src", type="string")
     * @var string
     */
    private $src;

    /**
     * in|out|missed|rejected|conference|unaccepted
     *
     * @ORM\Column(name="type", type="string")
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(name="datetime", type="string")
     * @var string
     */
    private $datetime;

    /**
     * @ORM\Column(name="duration", type="integer")
     * @var int
     */
    private $duration;

    /**
     * @ORM\Column(name="billsec", type="integer")
     * @var int
     */
    private $billsec;

    /**
     * @ORM\Column(name="record_id", type="string")
     * @var int
     */
    private $record_id;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return new \DateTime($this->datetime);
    }


}