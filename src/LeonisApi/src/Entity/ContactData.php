<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LeonisApi\Repository\ContactDataRepository")
 * @ORM\Table(name="contact_data")
 */
class ContactData
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="contact_id", type="integer")
     * @var int
     */
    private $contact_id;

    /**
     * @ORM\Column(name="field", type="string", length=32)
     * @var string
     */
    private $field;

    /**
     * @ORM\Column(name="ext", type="string", length=32)
     * @var string
     */
    private $ext;

    /**
     * @ORM\Column(name="value", type="string", length=255)
     * @var string
     */
    private $value;

    /**
     * @ORM\Column(name="sort", type="integer")
     * @var string
     */
    private $sort;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getContactId(): int
    {
        return $this->contact_id;
    }

    /**
     * @param int $contact_id
     */
    public function setContactId(int $contact_id)
    {
        $this->contact_id = $contact_id;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField(string $field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getExt(): string
    {
        return $this->ext;
    }

    /**
     * @param string $ext
     */
    public function setExt(string $ext)
    {
        $this->ext = $ext;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort(string $sort)
    {
        $this->sort = $sort;
    }
}
