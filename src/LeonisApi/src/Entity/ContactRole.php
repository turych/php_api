<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="crm_contact_role")
 */
class ContactRole
{
    /**
     * @ORM\OneToOne(targetEntity="Contact", inversedBy="role_id")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * @var int
     */
    private $contact_id;

    /**
     * @ORM\Id
     * @ORM\Column(name="role_id", type="integer")
     * @var int
     */
    private $role_id;

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }
}
