<?php

namespace LeonisApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shop_currency")
 */

class Currency
{
    /**
     * @ORM\Id
     * @ORM\Column(name="code", type="string", length=3)
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(name="rate", type="decimal")
     * @var int
     */
    private $rate;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }
}