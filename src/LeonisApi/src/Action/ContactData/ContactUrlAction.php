<?php

namespace LeonisApi\Action\ContactData;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\ContactData;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

/**
 * Class ContactUrlAction
 * @package LeonisApi\Action
 *
 * CRUD for contact`s site urls.
 */
class ContactUrlAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

    private $table_field = 'url';

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    /**
     * Create data.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $last_row = $this->entityManager->getRepository(ContactData::class)
                            ->findBy(
                                [
                                    'contact_id' => $contact_id,
                                    'field' => $this->table_field
                                ],
                                ['sort' => 'DESC'],
                                1
                            );
        $last_sort = is_null($last_row) ? 0 : $last_row[0]->getSort() + 1;

        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            foreach ($body as $item) {
                $data = new ContactData();
                $data->setContactId($contact_id);
                $data->setField($this->table_field);
                $data->setValue($item['value']);
                $data->setExt($item['ext']);
                $data->setSort($last_sort++);
                $this->entityManager->persist($data);
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
            return new JsonResponse(null, 201);
        } else {
            return new JsonResponse(null, 400);
        }

    }

    /**
     * Update data by sort. If data isn`t found by sort, ignored.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
	public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {

            foreach ($body as $item) {
                $data = $this->entityManager->getRepository(ContactData::class)
                                ->findOneBy([
                                    'contact_id' => $contact_id,
                                    'field' => $this->table_field,
                                    'sort' => $item['sort']
                                ]);
                if (!is_null($data)) {
                    $data->setValue($item['value']);
                    $data->setExt($item['ext']);
                    $this->entityManager->persist($data);
                }
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
        }

        return new JsonResponse(null);
    }

    /**
     * Deleting and resort other rows.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            $data = $this->entityManager->getRepository(ContactData::class)
                            ->findBy(
                                [
                                    'contact_id' => $contact_id,
                                    'field' => $this->table_field
                                ],
                                ['sort' => 'ASC']
                            );

            $sort = 0;
            if (count($data)) {
                foreach ($data as $d) {
                    if (in_array($d->getValue(), $body)) {
                        // Delete items
                        $this->entityManager->remove($d);
                        $this->entityManager->flush();
                    } else {
                        // Resort rows
                        $d->setSort($sort++);
                        $this->entityManager->persist($d);
                    }
                }
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            return new JsonResponse(null);
        } else {
            return new JsonResponse(null, 404);
        }
    }
}
