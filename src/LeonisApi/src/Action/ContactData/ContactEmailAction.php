<?php

namespace LeonisApi\Action\ContactData;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\ContactEmails;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

/**
 * Class ContactEmailAction
 * @package LeonisApi\Action
 *
 * CRUD for contact`s email.
 */
class ContactEmailAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    /**
     * Create emails.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $last_email = $this->entityManager->getRepository(ContactEmails::class)
                            ->findBy(
                                ['contact_id' => $contact_id],
                                ['sort' => 'DESC'],
                                1
                            );
        $last_sort = is_null($last_email) ? 0 : $last_email[0]->getSort() + 1;

        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            foreach ($body as $item) {
                $email = new ContactEmails();
                $email->setContactId($contact_id);
                $email->setEmail($item['value']);
                $email->setExt($item['ext']);
                $email->setSort($last_sort++);
                $this->entityManager->persist($email);
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
            return new JsonResponse(null, 201);
        } else {
            return new JsonResponse(null, 400);
        }

    }

    /**
     * Update emails by sort. If email isn`t found by sort, ignored.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
	public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {

            foreach ($body as $item) {
                $email = $this->entityManager->getRepository(ContactEmails::class)
                                ->findOneBy([
                                    'contact_id' => $contact_id,
                                    'sort' => $item['sort']
                                ]);
                if (!is_null($email)) {
                    $email->setEmail($item['value']);
                    $email->setExt($item['ext']);
                    $this->entityManager->persist($email);
                }
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
        }

        return new JsonResponse(null);
    }

    /**
     * Deleting and resort other rows.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            $emails = $this->entityManager->getRepository(ContactEmails::class)
                            ->findBy(
                                ['contact_id' => $contact_id],
                                ['sort' => 'ASC']
                            );

            $sort = 0;
            if (count($emails)) {
                foreach ($emails as $email) {
                    if (in_array($email->getEmail(), $body)) {
                        $this->entityManager->remove($email);
                        $this->entityManager->flush();
                    } else {
                        $email->setSort($sort++);
                        $this->entityManager->persist($email);
                    }
                }
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            return new JsonResponse(null);
        } else {
            return new JsonResponse(null, 404);
        }
    }
}
