<?php

namespace LeonisApi\Action\ContactData;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\ContactData;
use LeonisApi\Entity\Address;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

/**
 * Class ContactAddressAction
 * @package LeonisApi\Action
 *
 * CRUD for contact`s address.
 */
class ContactAddressAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

    private $table_field = 'address';

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    /**
     * Create data.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');

        $qb = $this->entityManager->createQueryBuilder();
        $dql = $qb->select('cd')
            ->from(ContactData::class, 'cd')
            ->where($qb->expr()->like('cd.field', "'address%'"))
            ->andWhere('cd.contact_id = :contact_id')
            ->setMaxResults(1)
            ->orderBy('cd.sort', 'DESC')
            ->setParameter('contact_id', $contact_id)
        ;
        $last_row = $dql->getQuery()->getOneOrNullResult();

        $last_sort = is_null($last_row) ? 0 : $last_row->getSort() + 1;

        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            foreach ($body as $address) {
                $ext = $address['ext'];
                unset($address['sort'], $address['ext']);
                foreach ($address as $field_name => $value) {
                    $data = new ContactData();
                    $data->setContactId($contact_id);
                    $data->setField($this->table_field . ":" . $field_name);
                    $data->setValue($value);
                    $data->setExt($ext);
                    $data->setSort($last_sort);
                    $this->entityManager->persist($data);
                }
                $last_sort++;
            }
            $this->entityManager->flush();
            $this->entityManager->clear();
            return new JsonResponse(null, 201);
        } else {
            return new JsonResponse(null, 400);
        }
    }

    /**
     * Update data by sort. If data isn`t found by sort, ignored.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
	public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {

            foreach ($body as $address) {

                $sort = $address['sort'];
                $ext = $address['ext'];
                unset($address['sort'], $address['ext']);

                $qb = $this->entityManager->createQueryBuilder();
                $dql = $qb->select('cd')
                    ->from(ContactData::class, 'cd')
                    ->where($qb->expr()->like('cd.field', "'address%'"))
                    ->andWhere('cd.contact_id = :contact_id')
                    ->andWhere('cd.sort = :sort')
                    ->setParameters([
                        'contact_id' => $contact_id,
                        'sort' => $sort
                    ])
                ;
                $address_data = $dql->getQuery()->getResult();

                if ($address_data) {
                    foreach ($address_data as $address_to_update) {
                        $address_to_update->setValue($address[str_replace(
                            'address:',
                            '',
                            $address_to_update->getField()
                        )]);
                        $address_to_update->setExt($ext);
                        $this->entityManager->persist($address_to_update);
                    }
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                } else {
                    return new JsonResponse(null, 404);
                }
            }
        } else {
            return new JsonResponse(null, 404);
        }
        return new JsonResponse(null);
    }

    /**
     * Deleting and resort other rows.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (is_array($body) && count($body)) {
            $addresses = $this->entityManager->getRepository(Address::class)->getAddress($contact_id);

            if (!is_null($addresses)) {
                $sort = 0;
                foreach ($addresses as $address) {
                    if (in_array($address->getSort(), $body)) {
                        // Delete items
                        $this->entityManager->getRepository(Address::class)
                                            ->deleteAddress($contact_id, $address->getSort());

                    } else {
                        // Resort rows
                        $this->entityManager->getRepository(Address::class)
                                            ->updateAddressSort($contact_id, $address->getSort(), $sort++);
                    }
                }
                return new JsonResponse(null);
            } else {
                return new JsonResponse(null, 404);
            }
        }
    }
}
