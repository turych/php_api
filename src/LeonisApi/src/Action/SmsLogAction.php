<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use LeonisApi\Entity\SmsLog;

class SmsLogAction implements ServerMiddlewareInterface
{
	use RestDispatchTrait;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $row_count = $this->entityManager->getRepository(SmsLog::class)->insertLogs($body);
        if ($row_count > 0) {
            return new JsonResponse(null,201);
        } else {
            return new JsonResponse(null, 500);
        }
    }
}
