<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use LeonisApi\Entity\CallLog;
use LeonisApi\Entity\ContactData;

class CallLogAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

    /**
     * CallLogAction constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getQueryParams();
        $identity = $request->getAttribute('Auth\Middleware\AuthenticationMiddleware');

        $contactPhones = $this->entityManager->getRepository(ContactData::class)
                                ->findBy([
                                    'contact_id' => $identity->getContactId(),
                                    'field' => 'phone'
                                ]);

        if (count($contactPhones) == 0) {
            return new JsonResponse(['errors' => ["Your account doesn`t have phone numbers."]], 400);
        }

        $phones = [];

        foreach ($contactPhones as $cp) {
            $phones[] = $cp->getValue();
        }

        if (isset($params['limit'], $params['limit'])) {

            if (!isset($params['date']['from'], $params['date']['to'])) {
                $params['date'] = null;
            }

            $call_log = $this->entityManager->getRepository(CallLog::class)
                ->getLogsByPhones($phones, $params['limit'], $params['offset'], $params['date']);

            return new JsonResponse($call_log);
        } else {
            return new JsonResponse(null, 400);
        }
    }

    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $row_count = $this->entityManager->getRepository(CallLog::class)->insertLogs($body);
        if ($row_count > 0) {
            return new JsonResponse(null,201);
        } else {
            return new JsonResponse(null, 500);
        }
    }
}
