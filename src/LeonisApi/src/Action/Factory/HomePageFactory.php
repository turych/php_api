<?php

namespace LeonisApi\Action\Factory;

use Interop\Container\ContainerInterface;
use LeonisApi\Action\HomePageAction;

class HomePageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new HomePageAction();
    }
}
