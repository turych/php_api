<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

use LeonisApi\Entity\Contact;

class ContactAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $params = $request->getQueryParams();

        $identity = $request->getAttribute('Auth\Middleware\AuthenticationMiddleware');

        if (!is_null($contact_id)) {
            
        	$contact = $this->entityManager
                            ->getRepository(Contact::class)
                            ->getOneContactById($contact_id, true);

        	if (is_null($contact)) {
        		return new JsonResponse([], 404);	
        	} else {
        		return new JsonResponse($contact);
        	}
        } elseif (count($params) > 1) {
    		return new JsonResponse(
        		$this->entityManager
                    ->getRepository(Contact::class)
                    ->getAll(
                        $params['offset'],
                        $params['limit'],
                        $identity->getRole() != 1 ? $identity->getContactId() : null,
                        true
                    )
    		);
        } else {
            return new JsonResponse(null, 400);
        }
    }

    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $identity = $request->getAttribute('Auth\Middleware\AuthenticationMiddleware');

    	$contact_id = $this->entityManager->getRepository(Contact::class)->insertOne($body, $identity->getContactId());

    	if ($contact_id) {
            $contact = $this->entityManager
                ->getRepository(Contact::class)
                ->getOneContactById($contact_id, true);
        } else {
            return new JsonResponse(null, 404);
        }

        if (is_null($contact)) {
            return new JsonResponse(null, 404);
        } else {
            return new JsonResponse($contact, 201);
        }
    }

    public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (!is_array($body) || !count($body) || is_null($contact_id)) {
            return new JsonResponse(null, 400);
        }

        $identity = $request->getAttribute('Auth\Middleware\AuthenticationMiddleware');

        $contact_id = $this->entityManager->getRepository(Contact::class)
                                        ->updateOne($contact_id, $body, $identity->getContactId());

        if (!$contact_id) {
            return new JsonResponse(null, 404);
        }

        return new JsonResponse(null, 200);
    }

    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contact_id = $request->getAttribute('id');

        $contact = $this->entityManager->getRepository(Contact::class)->find($contact_id);
        if (!$contact) {
            return new JsonResponse([], 404);
        } else {
            $contact->setStatus('deleted');
            $this->entityManager->persist($contact);
            $this->entityManager->flush();

            return new JsonResponse(
                ['messages' => ["Contact $contact_id deleted."]]
            );
        }
    }
}
