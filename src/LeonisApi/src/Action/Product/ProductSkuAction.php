<?php

namespace LeonisApi\Action\Product;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Entity\Currency;
use LeonisApi\Entity\Product\ProductStocks;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\{
    JsonResponse,
    EmptyResponse
};
use Doctrine\ORM\EntityManager;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\Product\Product;
use LeonisApi\Entity\Product\ProductSkus;
use LeonisApi\Helper\Transliterator;

class ProductSkuAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $sku_id = $request->getAttribute('sku_id');

        if (! $product_id || ! $sku_id) {
            return new EmptyResponse(404);
        }

        $sku = $this->entityManager->getRepository(ProductSkus::class)->findOneBy([
            'product_id' => $product_id,
            'id' => $sku_id
        ]);

        if ($sku) {
            return new JsonResponse([
                'id' => $sku->getId(),
                'product_id' => $sku->getProductId(),
                'sku' => $sku->getSku(),
                'name' => $sku->getName(),
                'price' => $sku->getPrice(),
                'primary_price' => $sku->getPrimaryPrice(),
                'purchase_price' => $sku->getPurchasePrice(),
                'count' => $sku->getCount(),
                'available' => $sku->getAvailable()
            ]);
        }

        return new EmptyResponse(404);
    }

    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $body = json_decode($request->getBody()->getContents(), true);
        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['id' => $product_id]);

        if (! $product) {
            return new EmptyResponse(404);
        }

        $sku = new ProductSkus();
        $sku->setProductId($product_id);
        $sku->setSku($body['sku']);
        $sku->setName($body['name']);
        $sku->setSort($body['sort']);
        $sku->setPrice($body['price']);
        $sku->setPrimaryPrice($body['primary_price']);
        $sku->setPurchasePrice($body['purchase_price']);
        $sku->setAvailable($body['available']);

        if (isset($body['count'])) {
            $sku->setCount($body['count']);
            $this->entityManager->getRepository(ProductStocks::class)->updateProductAndSkuCounts($product_id);
        }

        $currency = $this->entityManager->getRepository(Currency::class)->findOneBy(
            ['code' => $product->getCurrency()]
        );

        $min_price = $sku->getPrice() * $currency->getRate();
        if ($product->getMinPrice() > $min_price || $product->getMinPrice() == 0) {
            $product->setMinPrice($min_price);
        }

        $max_price = $sku->getPrice() * $currency->getRate();
        if ($product->getMaxPrice() < $max_price || $product->getMaxPrice() == 0) {
            $product->setMaxPrice($max_price);
        }

        if ($sku->getSort() == 1) {
            $product->setPrice($sku->getPrice() * $currency->getRate());
        }

        $this->entityManager->persist($product);
        $this->entityManager->persist($sku);
        $this->entityManager->flush();

        $location = 'https://' .
                    $request->getServerParams()['HTTP_HOST'] .
                    '/product/' . $product->getId() .
                    '/sku/' . $sku->getId();

        return new EmptyResponse(
            201,
            [
                'Location' => $location
            ]
        );
    }

    // TODO Create method in repository for insert & update Sku.
    public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $sku_id = $request->getAttribute('sku_id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (! $product_id || ! $sku_id) {
            return new EmptyResponse(404);
        }

        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['id' => $product_id]);
        if (! $product) {
            return new EmptyResponse(404);
        }

        $sku = $this->entityManager->getRepository(ProductSkus::class)->findOneBy([
            'product_id' => $product_id,
            'id' => $sku_id
        ]);
        if (! $sku) {
            return new EmptyResponse(404);
        }

        $sku->setSku($body['sku']);
        $sku->setName($body['name']);
        $sku->setSort($body['sort']);
        $sku->setPrice($body['price']);
        $sku->setPrimaryPrice($body['primary_price']);
        $sku->setPurchasePrice($body['purchase_price']);
        $sku->setAvailable($body['available']);

        if (isset($body['count'])) {
            $sku->setCount($body['count']);
            $this->entityManager->getRepository(ProductStocks::class)->updateProductAndSkuCounts($product_id);
        }

        $currency = $this->entityManager->getRepository(Currency::class)->findOneBy(
            ['code' => $product->getCurrency()]
        );

        $min_price = $sku->getPrice() * $currency->getRate();
        if ($product->getMinPrice() > $min_price || $product->getMinPrice() == 0) {
            $product->setMinPrice($min_price);
        }

        $max_price = $sku->getPrice() * $currency->getRate();
        if ($product->getMaxPrice() < $max_price || $product->getMaxPrice() == 0) {
            $product->setMaxPrice($max_price);
        }

        if ($sku->getSort() == 1) {
            $product->setPrice($sku->getPrice() * $currency->getRate());
        }

        $this->entityManager->persist($product);
        $this->entityManager->persist($sku);
        $this->entityManager->flush();

        return new EmptyResponse(204);
    }

}
