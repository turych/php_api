<?php

namespace LeonisApi\Action\Product;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Entity\Product\Product;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use LeonisApi\Action\RestDispatchTrait;

class ProductListAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getQueryParams();

        if (!isset($params['category_id'], $params['limit'], $params['offset'])) {
            return new JsonResponse('', 400);
        }

        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select('p')
                    ->from(Product::class, 'p')
                    ->where('p.category_id = :category_id')
                    ->andWhere('p.status = 1')
                    ->setParameter('category_id', $params['category_id'])
                    ->setMaxResults($params['limit'])
                    ->setFirstResult($params['offset'])
                    ->getQuery()
        ;

        $result = $query->getScalarResult();

        if (count($result)) {
            return new JsonResponse($result);
        }

        return new JsonResponse('', 404);
    }
}
