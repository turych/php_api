<?php

namespace LeonisApi\Action\Product;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\{
    JsonResponse,
    EmptyResponse
};
use Doctrine\ORM\EntityManager;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\Product\Product;
use LeonisApi\Entity\Product\ProductSkus;
use LeonisApi\Entity\Product\ProductStocks;

class ProductStockAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $sku_id = $request->getAttribute('sku_id');
        $stock_id = $request->getAttribute('stock_id');

        if (! $product_id || ! $sku_id || ! $stock_id) {
            return new EmptyResponse(404);
        }

        $stock = $this->entityManager->getRepository(ProductStocks::class)->findOneBy([
           'sku_id' => $sku_id,
           'stock_id' => $stock_id,
           'product_id' => $product_id
        ]);

        if (! $stock) {
            return new EmptyResponse(404);
        }

        return new JsonResponse([
            'stock_id' => $stock->getStockId(),
            'count' => $stock->getCount()
        ]);
    }

    public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $sku_id = $request->getAttribute('sku_id');
        $stock_id = $request->getAttribute('stock_id');
        $body = json_decode($request->getBody()->getContents(), true);

        if (! $product_id || ! $sku_id || ! $stock_id) {
            return new EmptyResponse(404);
        }

        $productStock = $this->entityManager->getRepository(ProductStocks::class)->findOneBy([
            'sku_id' => $sku_id,
            'stock_id' => $stock_id,
            'product_id' => $product_id
        ]);

        if (! $productStock) {
            $productStock = new ProductStocks();
            $productStock->setProductId($product_id);
            $productStock->setSkuId($sku_id);
            $productStock->setStockId($stock_id);
        }
        $productStock->setCount($body['count']);

        $this->entityManager->persist($productStock);
        $this->entityManager->flush();

        $this->entityManager->getRepository(ProductStocks::class)->updateProductAndSkuCounts($product_id);

        return new EmptyResponse(204);
    }

    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');
        $sku_id = $request->getAttribute('sku_id');
        $stock_id = $request->getAttribute('stock_id');

        $productStock = $this->entityManager->getRepository(ProductStocks::class)->findOneBy([
            'sku_id' => $sku_id,
            'stock_id' => $stock_id,
            'product_id' => $product_id
        ]);

        if (! $productStock) {
            return new EmptyResponse(404);
        }

        $this->entityManager->remove($productStock);

        return new EmptyResponse(204);
    }

}
