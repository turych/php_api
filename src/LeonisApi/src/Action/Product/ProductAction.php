<?php

// TODO Add features to full product info

namespace LeonisApi\Action\Product;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Entity\Product\ProductStocks;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\{
    JsonResponse,
    EmptyResponse
};
use Doctrine\ORM\EntityManager;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\Product\Product;
use LeonisApi\Entity\Product\ProductSkus;
use LeonisApi\Helper\Transliterator;

class ProductAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');

        if (! $product_id) {
            return new JsonResponse('', 404);
        }

        $query = $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(Product::class, 'p')
            ->where('p.id = :id')
            ->andWhere('p.status = 1')
            ->setParameter('id', $product_id)
            ->getQuery()
        ;

        $product = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if ($product) {

            $query = $this->entityManager->createQueryBuilder()
                ->select('ps')
                ->from(ProductSkus::class, 'ps')
                ->where('ps.product_id = :product_id')
                ->setParameter('product_id', $product_id)
                ->orderBy('ps.sort')
                ->getQuery()
            ;

            $skus = $query->getArrayResult();

            if ($skus) {
                foreach ($skus as &$sku) {
                    $stocks = $this->entityManager->getRepository(ProductStocks::class)->findBy([
                        'sku_id' => $sku['id']
                    ]);
                    if ($stocks) {
                        foreach ($stocks as $stock) {
                            $sku['stocks'][] = [
                                'stock_id' => $stock->getStockId(),
                                'count' => $stock->getCount()
                            ];
                        }
                    }
                }
                unset($sku);
            }

            $product['skus'] = $skus;

            $this->entityManager->getRepository(Product::class)->getFeaturesAndValues($product_id);

            return new JsonResponse($product);
        } else {
            return new EmptyResponse(404);
        }
    }

    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $body = json_decode($request->getBody()->getContents(), true);

        $product = new Product();
        $product->setName($body['name']);
        $product->setDescription($body['description']);
        $product->setStatus($body['status']);
        $product->setCurrency($body['currency']);
        $product->setCategoryId($body['category_id']);
        $product->setUrl(Transliterator::transliterate($body['name']));

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return new EmptyResponse(
            201,
            [
                'Location' => 'https://' . $request->getServerParams()['HTTP_HOST'] . '/product/' . $product->getId()
            ]
        );
    }

    public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $product_id = $request->getAttribute('product_id');

        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['id' => $product_id]);

        if (! $product) {
            return new EmptyResponse(404);
        }

        $body = json_decode($request->getBody()->getContents(), true);

        $product->setName($body['name']);
        $product->setDescription($body['description']);
        $product->setStatus($body['status']);
        $product->setCurrency($body['currency']);
        $product->setCategoryId($body['category_id']);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return new EmptyResponse(204);
    }

}
