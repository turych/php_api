<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.01.18
 * Time: 15:56
 */

namespace LeonisApi\Action\Order;

use FastRoute\BadRouteException;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\Order\Order;
use LeonisApi\Entity\Order\OrderItems;
use LeonisApi\Entity\Product\Product;
use LeonisApi\Repository\ContactRepository;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use LeonisApi\Entity\Contact;


class OrderAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $order_id = $request->getAttribute('id');
        $result = $this->entityManager
            ->getRepository(Order::class)
            ->getByIdWithItems($order_id);
        return new JsonResponse($result);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $data = json_decode($request->getBody()->getContents(), true);
        $contact = $this->entityManager->getRepository(Contact::class)->find($data['contact_id'] ?? 0);
        $assigned = $this->entityManager->getRepository(Contact::class)->find($data['assigned_contact_id'] ?? 0);

        $order = new Order();
        $order->setCreateDatetime()
            ->setTotal($data['total'] ?? null)
            ->setContact($contact)
            ->setAssignedContact($assigned)
            ->setCurrency($data['currency'] ?? 'UAH')
            ->setRate($data['rate'] ?? 1)
            ->setComment($data['comment'] ?? null)
            ->setDiscount($data['discount'] ?? 0)
            ->setShipping($data['shipping'] ?? 0)
            ->setTax($data['tax'] ?? 0);

        $this->entityManager->persist($order);
        $this->entityManager->flush();
        foreach ($data['items'] as $item) {
            if (empty($item['product_id'])) {
                continue;
            }
            $order_item = new OrderItems();
            $product = $this->entityManager->getRepository(Product::class)->find($item['product_id']);
            $order_item->setOrder($order)
                ->setItem($product)
                ->setName($product->getName())
                ->setSkuId($item['sku_id'] ?? $product->getSkuId())
                ->setSkuCode($item['sku_code'] ?? '')
                ->setType($item['type'] ?? 'product')
                ->setServiceId($item['service_id'] ?? null)
                ->setServiceVariantId($item['service_variant_id'] ?? null)
                ->setPrice($item['price'] ?? $product->getPrice())
                ->setQuantity($item['quantity'] ?? 1)
                ->setParentId($item['parent_id'] ?? null)
                ->setStockId($item['stock_id'] ?? null)
                ->setPurchasePrice($item['purchase_price'] ?? 0);
            $this->entityManager->persist($order_item);
        }
        $this->entityManager->flush();
        return new JsonResponse(['order_id' => $order->getId()]);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return EmptyResponse|JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function doPut(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $data = json_decode($request->getBody()->getContents(), true);
        $order_id = $request->getAttribute('order_id');
        /* @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->find($order_id);
        if (!$order) {
            return new EmptyResponse(404);
        }
        foreach ($data['items'] as $item) {
            $order_item = $this->entityManager->getRepository(OrderItems::class)
                ->getItemByOrderIdAndItemId($order_id, $item['product_id']);
            $product = $this->entityManager->getRepository(Product::class)->find($item['product_id']);
            $new_item = new OrderItems();
            $new_item->setOrder($order)
                ->setItem($product)
                ->setName($product->getName())
                ->setSkuId($item['sku_id'] ?? $product->getSkuId())
                ->setSkuCode($item['sku_code'] ?? '')
                ->setType($item['type'] ?? 'product')
                ->setServiceId($item['service_id'] ?? null)
                ->setServiceVariantId($item['service_variant_id'] ?? null)
                ->setPrice($item['price'] ?? $product->getPrice())
                ->setQuantity($item['quantity'])
                ->setParentId($item['parent_id'] ?? null)
                ->setStockId($item['stock_id'] ?? null)
                ->setPurchasePrice($item['purchase_price'] ?? 0);
            if ($order_item) {
                $new_item->setOrder($order)->setId($order_item[0]->getId());
                $order->changeSum($order_item[0]->getSum(), $new_item->getSum());
                $this->entityManager->merge($new_item);
            } else {
                $order->addSum($new_item->getSum());
                $this->entityManager->persist($new_item);
            }
        }
        $order->setUpdateDatetime();
        $this->entityManager->merge($order);
        $this->entityManager->flush();
        return new JsonResponse($order->getAsArray());
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return EmptyResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function doDelete(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $item_id = $request->getAttribute('item_id');
        $order_id = $request->getAttribute('order_id');
        /* @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->find($order_id);
        $order_item = $this->entityManager->getRepository(OrderItems::class)->getItemByOrderIdAndItemId($order_id, $item_id);
        if (!$order_item) {
            return new EmptyResponse(404);
        }
        $order->changeSum($order_item[0]->getSum());
        $this->entityManager->remove($order_item[0]);
        $this->entityManager->merge($order);
        $this->entityManager->flush();
        return new EmptyResponse(200);
    }

}