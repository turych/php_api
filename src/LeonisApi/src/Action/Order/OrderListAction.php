<?php
/**
 * Created by PhpStorm.
 * User: DartVadius
 * Date: 25.01.18
 * Time: 16:12
 */

namespace LeonisApi\Action\Order;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use LeonisApi\Action\RestDispatchTrait;
use LeonisApi\Entity\Order\Order;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

class OrderListAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    const OFFSET = 0;
    const LIMIT = 20;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {

        $params = $request->getQueryParams();
        $result = $this->entityManager
            ->getRepository(Order::class)
            ->getAll(
                $params['offset'] ?? self::OFFSET,
                $params['limit'] ?? self::LIMIT,
                $params['contact_id'] ?? null,
                $params['manager_id'] ?? null,
                $params['date_from'] ?? null,
                $params['date_to'] ?? null,
                $params['status'] ?? null
            );
        return new JsonResponse($result);
    }
}