<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

use LeonisApi\Entity\Category;

class CategoryAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $category_id = $request->getAttribute('id');

        return new JsonResponse('', 404);
    }
}
