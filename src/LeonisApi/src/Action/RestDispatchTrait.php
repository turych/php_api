<?php
namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

trait RestDispatchTrait
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $method = "do" . ucfirst(strtolower($request->getMethod()));
        if (method_exists($this, $method)) {
            return $this->$method($request, $delegate);
        }
        return new JsonResponse([], 501);
    }
}