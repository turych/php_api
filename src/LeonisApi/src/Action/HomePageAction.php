<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class HomePageAction implements ServerMiddlewareInterface
{
	use RestDispatchTrait;

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        return new JsonResponse([
            'welcome' => 'Here can be your advertising',
        ], 418);
    }
}
