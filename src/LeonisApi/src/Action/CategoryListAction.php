<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;

use LeonisApi\Entity\Category;

class CategoryListAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

    public function doGet(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select('c')
                    ->from(Category::class, 'c')
                    ->where('c.status = :status')
                    ->setParameter('status', 1)
                    ->getQuery()
        ;

        $categories = $query->getArrayResult();

        if (count($categories)) {
            return new JsonResponse($categories);
        }

        return new JsonResponse('', 404);
    }
}
