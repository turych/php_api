<?php

namespace LeonisApi\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use LeonisApi\Entity\CallLog;

class CallRecordAction implements ServerMiddlewareInterface
{
    use RestDispatchTrait;

    private $entityManager;

    private $record_ext = ".mp3";

    private $records_path = PUBLIC_PATH . "/records";
    /**
     * CallLogAction constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function doPost(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $record_id = $request->getAttribute('id');

        if (is_null($record_id)) {
            return new JsonResponse(['id'], 406);
        }

        $file = $request->getBody()->getContents();

        if (strlen($file) == 0) {
            return new JsonResponse(null, 400);
        }

        $log = $this->entityManager->getRepository(CallLog::class)->findOneBy(['record_id' => $record_id]);

        if (is_null($log)) {
            return new JsonResponse(null, 404);
        }

        $path = [
            $this->records_path,
            $log->getDate()->format('Y'),
            $log->getDate()->format('m'),
            $log->getDate()->format('d'),

        ];

        mkdir(implode("/", $path), 0755, true);
        array_push($path, $record_id . $this->record_ext);

        file_put_contents(implode("/", $path), $file);

        return new JsonResponse(null,201);
    }
}
