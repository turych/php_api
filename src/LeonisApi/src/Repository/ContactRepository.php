<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\{
    Contact,
    ContactData,
    ContactEmails,
    ManagerClients
};

class ContactRepository extends EntityRepository
{
    /**
     * Get one contact by ID.
     *
     * @param  integer $id
     * @param  boolean $fullInfo Add to contact data & emails.
     *
     * @return array
     */
    public function getOneContactById($id, $fullInfo = false)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select(['c.id', 'c.firstname', 'c.lastname', 'c.middlename', 'c.company', 'c.jobtitle', 'c.about'])
            ->from(Contact::class, 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        $result = $query->getArrayResult();
        $contact = count($result) > 0 ? $result[0] : null;

        if (is_null($contact)) {
            return null;
        }

        if ($fullInfo) {
            $contact['email'] = $this->getEmailsByContactId($id);
            $contact = array_merge($contact, $this->getDataByContactId($id));
        }

        return $contact;
    }

    /**
     * Get contact by pagination.
     *
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $assign_contact_id Get only contacts in my book.
     * @param  boolean $fullInfo Add to contact data & emails.
     *
     * @return array
     */
    public function getAll($offset, $limit, $assign_contact_id = null, $fullInfo = false)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        if (is_null($assign_contact_id)) {
            $qb->select(['c.id', 'c.firstname', 'c.lastname', 'c.middlename', 'c.company', 'c.jobtitle', 'c.about'])
                ->from(Contact::class, 'c');
        } else {
            $qb->select(['c.id', 'c.firstname', 'c.lastname', 'c.middlename', 'c.company', 'c.jobtitle', 'c.about'])
                ->from(Contact::class, 'c')
                ->leftJoin(
                    ManagerClients::class,
                    'mc',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'c.id = mc.contact_id'
                )
                ->where('mc.manager_id = :assign_contact_id')
                ->setParameter('assign_contact_id', $assign_contact_id);
        }

        $query = $qb->setFirstResult($offset)
            ->setMaxResults($limit)
            ->andWhere('c.status != :status')
            ->setParameter('status', 'deleted')
            ->getQuery();
        $contacts = $query->getArrayResult();

        if ($fullInfo) {
            foreach ($contacts as &$c) {
                $c['email'] = $this->getEmailsByContactId($c['id']);
                $c = array_merge($c, $this->getDataByContactId($c['id']));
            }
        }
        unset($c);

        return $contacts;
    }

    /**
     * @return array
     */
    public function getEmailsByContactId($id)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from(ContactEmails::class, 'e')
            ->where('e.contact_id = :contact_id')
            ->setParameter('contact_id', $id)
            ->getQuery();
        return $query->getArrayResult();
    }

    /**
     * @return array
     */
    public function getDataByContactId($id)
    {
        $data = [
            'phone' => [],
            'im' => [],
            'url' => [],
            'socialnetwork' => [],
            'address' => []
        ];

        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('d')
            ->from(ContactData::class, 'd')
            ->where('d.contact_id = :contact_id')
            ->setParameter('contact_id', $id)
            ->getQuery();

        $result = $query->getResult();

        if (count($result) > 0) {
            foreach ($result as $cd) {
                if ($address_field = substr(strstr($cd->getField(), ":"), 1)) {
                    $data['address'][$cd->getSort()][$address_field] = $cd->getValue();
                    $data['address'][$cd->getSort()]['sort'] = $cd->getSort();
                    $data['address'][$cd->getSort()]['ext'] = $cd->getExt();
                } else {
                    $data[$cd->getField()][$cd->getSort()] = [
                        'value' => $cd->getValue(),
                        'ext' => $cd->getExt(),
                        'sort' => $cd->getSort()
                    ];
                }
            }
        }
        return $data;
    }

    public function insertOne(array $data, $assign_contact_id)
    {
        $contact = new Contact();
        $contact->setFirstname($data['firstname']);
        $contact->setLastname($data['lastname']);
        $contact->setMiddlename($data['middlename']);
        $contact->setCompany($data['company']);
        $contact->setJobtitle($data['jobtitle']);
        $contact->setAbout($data['about']);
        $contact->setCreateContactId($assign_contact_id);

        $this->getEntityManager()->persist($contact);
        $this->getEntityManager()->flush();

        $managersClients = new ManagerClients();
        $managersClients->setContactId($contact->getId());
        $managersClients->setManagerId($assign_contact_id);

        $this->getEntityManager()->persist($managersClients);
        $this->getEntityManager()->flush();

        //TODO Create method for email & data and move this code.

        $items_to_insert = [];

        if (count($data['email'])) {
            foreach ($data['email'] as $e) {
                $email = new ContactEmails();
                $email->setContactId($contact->getId());
                $email->setEmail($e['email']);
                $email->setExt($e['ext']);
                $email->setSort($e['sort']);
                array_push($items_to_insert, $email);
            }
        }

        if (count($data['phone'])) {
            foreach ($data['phone'] as $c_d) {
                array_push($items_to_insert, $this->createDataEntity($c_d, 'phone', $contact->getId()));
            }
        }

        if (count($data['im'])) {
            foreach ($data['im'] as $c_d) {
                array_push($items_to_insert, $this->createDataEntity($c_d, 'im', $contact->getId()));
            }
        }

        if (count($data['url'])) {
            foreach ($data['url'] as $c_d) {
                array_push($items_to_insert, $this->createDataEntity($c_d, 'url', $contact->getId()));
            }
        }

        if (count($data['socialnetwork'])) {
            foreach ($data['socialnetwork'] as $c_d) {
                array_push($items_to_insert, $this->createDataEntity($c_d, 'socialnetwork', $contact->getId()));
            }
        }

        if (count($data['address'])) {
            foreach ($data['address'] as $c_a) {
                $sort = $c_a['sort'];
                $ext = $c_a['ext'];
                unset($c_a['sort'], $c_a['ext']);

                foreach ($c_a as $field_name => $c_a_item) {
                    if (!empty($c_a_item)) {
                        array_push($items_to_insert, $this->createDataEntity(
                            [
                                'value' => $c_a_item,
                                'sort' => $sort,
                                'ext' => $ext
                            ],
                            'address:' . $field_name,
                            $contact->getId()
                        ));
                    }
                }
            }
        }

        if (count($items_to_insert)) {
            $this->multipleFlust($items_to_insert, 'persist');
        }

        return $contact->getId();
    }

    public function updateOne($contact_id, array $data, $assign_contact_id)
    {
        $contact = $this->getEntityManager()->getRepository(Contact::class)->find($contact_id);
        if (is_null($contact)) {
            return false;
        }

        $assign_contact = $this->getEntityManager()
            ->getRepository(ManagerClients::class)
            ->findOneBy([
                'contact_id' => $contact_id,
                'manager_id' => $assign_contact_id
            ]);

        if (is_null($assign_contact)) {
            return false;
        }

        $contact->setFirstname($data['firstname']);
        $contact->setLastname($data['lastname']);
        $contact->setMiddlename($data['middlename']);
        $contact->setCompany($data['company']);
        $contact->setJobtitle($data['jobtitle']);

        $this->getEntityManager()->merge($contact);
        $this->getEntityManager()->flush();

        return $contact->getId();
    }

    /**
     * @param array $items
     * @param string $method persist|merge
     * @param int $batchSize
     */
    public function multipleFlust($items, $method, $batchSize = 20)
    {
        foreach ($items as $i => $item) {

            $this->getEntityManager()->$method($item);

            // flush everything to the database every 20 inserts
            if (($i % $batchSize) == 0) {
                $this->getEntityManager()->flush();
                $this->getEntityManager()->clear();
            }
        }

        // flush the remaining objects
        $this->getEntityManager()->flush();
        $this->getEntityManager()->clear();
    }

    /**
     * @param $data
     * @param $field_name
     * @param $contact_id
     * @return ContactData
     */
    public function createDataEntity($data, $field_name, $contact_id)
    {
        $c_data = new ContactData();
        $c_data->setContactId($contact_id);
        $c_data->setField($field_name);
        $c_data->setValue($data['value']);
        $c_data->setExt($data['ext']);
        $c_data->setSort($data['sort']);
        return $c_data;
    }
}