<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.01.18
 * Time: 11:02
 */

namespace LeonisApi\Repository;


use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\Order\OrderItems;

class OrderItemsRepository extends EntityRepository
{
    public function getItemsByOrderId($id)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $res = $query->select('o')
            ->from(OrderItems::class, 'o')
            ->andWhere('o.order_id = :order_id')
            ->setParameter('order_id', $id)
            ->getQuery();

        return $res->getResult();
    }

    public function getItemById($id)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $res = $query->select('o')
            ->from(OrderItems::class, 'o')
            ->andWhere('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $res->getResult();
    }

    /**
     * @param int $order_id
     * @param int $product_id
     * @param int|null $sku_id
     * @return mixed
     */
    public function getItemByOrderIdAndItemId($order_id, $product_id, $sku_id = null)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('o')
            ->from(OrderItems::class, 'o')
            ->andWhere('o.order_id = :order_id')
            ->andWhere('o.product_id = :product_id');
        if (!empty($sku_id)) {
            $query->andWhere('o.sku_id = :sku_id')
                ->setParameter('sku_id', $sku_id);
        }
        $res = $query->setParameter('order_id', $order_id)
            ->setParameter('product_id', $product_id)
            ->setMaxResults(1)
            ->getQuery();

        return $res->getResult();
    }

}