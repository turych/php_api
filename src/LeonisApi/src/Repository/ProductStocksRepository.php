<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\Product\{
    Product,
    ProductSkus,
    ProductStocks
};

class ProductStocksRepository extends EntityRepository
{
    /**
     * Update stock counts in product adn sku table.
     *
     * @param int $product_id
     */
    public function updateProductAndSkuCounts(int $product_id)
    {
        $stocks = $this->getEntityManager()
                        ->getRepository($this->getEntityName())
                        ->findBy(['product_id' => $product_id]);

        if ($stocks) {
            // Update product skus count.
            $dql = "UPDATE " . ProductSkus::class . " skus " .
                "SET skus.count = (
                    SELECT SUM(stocks.count) FROM " . ProductStocks::class . " stocks WHERE stocks.sku_id = skus.id
                ) 
                WHERE skus.product_id = :product_id";

            $query = $this->getEntityManager()->createQuery($dql);
            $query->setParameter('product_id', $product_id);
            $query->execute();
        }

        // Update product count.
        $dql = "UPDATE " . Product::class . " p " .
                "SET p.count = (
                    SELECT SUM(skus.count) FROM " . ProductSkus::class . " skus WHERE skus.product_id = :product_id
                ) 
                WHERE p.id = :product_id";

        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('product_id', $product_id);
        $query->execute();
    }
}