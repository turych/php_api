<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\CallLog;

/**
 * Class CallLogRepository
 * @package LeonisApi\Repository
 */
class CallLogRepository extends EntityRepository
{
    /**
     * @param array $phones
     * @param int $limit
     * @param null $offset
     * @param array $date
     * @return array
     */
    public function getLogsByPhones(array $phones, $limit = 1000, $offset = null, array $date = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $dql = $qb->select('cl')
            ->from(CallLog::class, 'cl')
            ->where($qb->expr()->in('cl.phone_id', $phones))
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('cl.datetime', 'DESC')
        ;

        if (!is_null($date)) {
            $dql->andWhere($qb->expr()->between(
                'cl.datetime',
                ':from',
                ':to'
            ))
            ->setParameters([
                'from' => $date['from'],
                'to' => $date['to']
            ]);
        }

        $result = $dql->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @param array $logs
     * @return int
     */
    public function insertLogs(array $logs)
    {
        $placeholders = $values = $types = [];

        foreach ($logs as $columnName => $value) {
            $_values = [
                'phone_id' => $value['phone_id'],
                'src' => $value['src'],
                'type' => $value['type'],
                'datetime' => $value['datetime'],
                'duration' => $value['duration'],
                'billsec' => $value['billsec'],
                'record_id' => $value['record_id']
            ];
            $placeholders[] = '(?)';
            $values[] = array_values($_values);
            $types[] = \Doctrine\DBAL\Connection::PARAM_INT_ARRAY;
        }

        $dql = "INSERT INTO crm_call_log(phone_id,src,type,datetime,duration,billsec,record_id) VALUES " . implode(', ', $placeholders);

        $res = $this->getEntityManager()->getConnection()->executeQuery($dql, $values, $types);
        return $res->rowCount();
    }
}