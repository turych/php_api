<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.01.18
 * Time: 11:07
 */

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\Order\Order;

class OrderRepository extends EntityRepository
{
    /**
     * @param int $id order id
     * @return array
     */
    public function getOrderById(int $id)
    {
        $query = $this->getEntityManager()->getRepository(self::getEntityName());
        $result = $query->find($id);

        return $result->getAsArray();

//        $query = $this->getEntityManager()->createQueryBuilder()
//            ->select('c')
//            ->from(Order::class, 'c')
//            ->where('c.id = :id')
//            ->setParameter('id', $id)
//            ->getQuery();
//
//        $result = $query->getArrayResult();
//        return $result;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getByIdWithItems($id)
    {
        $query = $this->getEntityManager()->getRepository(self::getEntityName());
        $result = $query->find($id);
        $array = $result->getAsArray();
        foreach ($result->getItems() as $item) {
            $array['items'][] = $item->getAsArray();
        }
        return $array;
    }



    public function getAll($offset, $limit, $contact_id = null, $manager_id = null, $date_from = null, $date_to = null, $status = null)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o');
        if ($contact_id) {
            $query->andWhere('o.contact_id = :contact_id')
                ->setParameter('contact_id', $contact_id);
        }
        if ($manager_id) {
            $query->andWhere('o.assigned_contact_id = :manager_id')
                ->setParameter('manager_id', $manager_id);
        }

        if ($date_from) {
            $query->andWhere('o.create_datetime >= :date_from')
                ->setParameter('date_from', $date_from);
        }

        if ($date_to) {
            $date = new \DateTime($date_to);
            $date->modify('+1 day');
            $query->andWhere('o.create_datetime <= :date_to')
                ->setParameter('date_to', $date->format('Y-m-d'));
        }

        if ($status) {
            $query->andWhere('o.state_id = :status')
                ->setParameter('status', $status);
        }

        $dc = $query->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery();

        return $dc->getScalarResult();

    }
}