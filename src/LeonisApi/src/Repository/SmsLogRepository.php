<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\SmsLog;

/**
 * Class CallLogRepository
 * @package LeonisApi\Repository
 */
class SmsLogRepository extends EntityRepository
{
    /**
     * @param array $logs
     * @return int
     */
    public function insertLogs(array $logs)
    {
        $placeholders = $values = $types = [];

        $smsLog = new SmsLog();

        foreach ($logs as $columnName => $value) {
            $_values = [
                'phone_id' => $value['phone_id'],
                'src' => $value['src'],
                'type' => $value['type'],
                'content' => $value['content'],
                'datetime' => $value['datetime'],
                'gateway_id' => $smsLog->getGatewayId()
            ];
            $placeholders[] = '(?)';
            $values[] = array_values($_values);
            $types[] = \Doctrine\DBAL\Connection::PARAM_INT_ARRAY;
        }

        $dql = "INSERT INTO crm_sms_statistics(phone_id,src,type,content,datetime,gateway_id) VALUES " . implode(', ', $placeholders);

        $res = $this->getEntityManager()->getConnection()->executeQuery($dql, $values, $types);
        return $res->rowCount();
    }
}