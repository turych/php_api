<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use LeonisApi\Entity\ContactData;
use LeonisApi\Entity\Address;

/**
 * Class ContactDataRepository
 * @package LeonisApi\Repository
 */
class ContactDataRepository extends EntityRepository
{
    /**
     * Convert ContactData row with address to Address entity.
     *
     * @param $contact_id
     * @return array|null
     */
    public function getAddress($contact_id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $dql = $qb->select('cd')
                    ->from(ContactData::class, 'cd')
                    ->where($qb->expr()->like('cd.field', "'address%'"))
                    ->andWhere('cd.contact_id = :contact_id')
                    ->orderBy('cd.sort', 'ASC')
                    ->setParameter('contact_id', $contact_id)
        ;

        $result = $dql->getQuery()->getResult();

        $addresses = [];
        if (count($result)) {
            foreach ($result as $row) {
                if (!isset($addresses[$row->getSort()])) {
                    $addresses[$row->getSort()] = new Address();
                    $addresses[$row->getSort()]->setSort($row->getSort());
                    $addresses[$row->getSort()]->setExt($row->getExt());
                }
                $field_method = 'set' . ucfirst(str_replace('address:', '', $row->getField()));
                $addresses[$row->getSort()]->$field_method($row->getValue());
            }
            return $addresses;
        }
        return null;
    }

    public function updateAddressSort($contact_id, $old_sort, $new_sort)
    {
        $sql = "UPDATE ". ContactData::class ." cd SET cd.sort = :new_sort WHERE cd.sort = :old_sort AND cd.contact_id = :contact_id";
        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters([
            'new_sort' => $new_sort,
            'old_sort' => $old_sort,
            'contact_id' => $contact_id
        ]);
        return $query->getResult();
    }

    public function deleteAddress($contact_id, $sort)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $dql = $qb->delete(ContactData::class, 'cd')
            ->where($qb->expr()->like('cd.field', "'address%'"))
            ->andWhere('cd.contact_id = :contact_id')
            ->andWhere('cd.sort = :sort')
            ->setParameter('contact_id', $contact_id)
            ->setParameter('sort', $sort)
        ;

        return $dql->getQuery()->getResult();
    }
}