<?php

namespace LeonisApi\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use LeonisApi\Entity\{
    Product\ProductFeatures,
    Feature\Feature
};

class ProductRepository extends EntityRepository
{
    public function getFeaturesAndValues(int $product_id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select([
                        'pf.feature_id',
                        'pf.feature_value_id',
                        'f.parent_id',
                        'f.code',
                        'f.status',
                        'f.name',
                        'f.type',
                        'f.selectable',
                        'f.multiple'
                    ])
                    ->from(ProductFeatures::class, 'pf')
                    ->leftJoin(
                        Feature::class,
                        'f',
                        \Doctrine\ORM\Query\Expr\Join::WITH,
                        'pf.feature_id = f.id'
                    )
                    ->where('pf.product_id = :product_id')
                    ->setParameter('product_id', $product_id)
                    ->getQuery()
        ;

        $features = $query->getArrayResult();

        var_dump($features); die;
    }
}