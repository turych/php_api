<?php
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params' => [
                    'url' => 'mysql://root:1111@localhost/test',
                    'driverOptions' => array(
                        1002 => 'SET NAMES utf8'
                    )
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
                    'Auth\Entity' => 'my_entity',
                    'LeonisApi\Entity' => 'my_entity',
                ],
            ],
            'my_entity' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../src/Auth/Entity',
                    __DIR__ . '/../../src/LeonisApi/Entity',
                ]
            ],
        ],
    ],
];