<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once 'container.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = $container->get('doctrine.entity_manager.orm_default');

return ConsoleRunner::createHelperSet($entityManager);
